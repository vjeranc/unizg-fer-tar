import sys

f = open(sys.argv[1],'r')

f1 = open("rijeci","r")
f2 = open("freqParovi","r")

fB = open("paths","r")

snomed_shortened = open('/home/vujevic/Downloads/clinical/izlaz')
expr_dict = set()

for line in snomed_shortened:
    _, b = line.split(maxsplit=1)
    expr_dict.update(b.split())


def in_dict(x):
    if x in expr_dict:
        return "1"
    else:
        return "0"


document = []


sentence = []
tags = []

freq = {}
freqT = {}

prev = ""

bDict = {}

for line in fB:
    arr = line.strip().split("\t")
    bDict[arr[1]] = arr[0]

for line in f:
    if len(line.strip()) < 1:
        document.append(sentence)
        sentence = []
        prev = ""
    else:
        arr = line.strip().split("\t")
        if len(arr) < 2:
            sentence.append(arr)
            w = arr[0]
        else:
            w = arr[0].strip()
            t = arr[1].strip()
            # freq[w] = freq.get(w,0) + 1
            sentence.append((w,t))
            '''if prev:
                freqT[(prev,w)] = freqT.get((prev,w),0) + 1
                prev = w
            else:
                prev = w

            '''
for line in f1:
    arr = line.strip().split("\t")
    freq[arr[0]] = int(arr[1])

for line in f2:
    arr = line.strip().split("\t")
    freqT[(arr[0],arr[1])] = int(arr[2])

''' for key,value in freq.items():
    f1.write(key + "\t" + str(value) + "\n")

for tup,value in freqT.items():
    f2.write(tup[0] + "\t" + tup[1] + "\t" + str(value) + "\n")
'''

for sent in document[:int(len(document))]:
    for i in range(0,len(sent)):
        if len(sent[i]) == 2:
            word = sent[i][0]
            postag = sent[i][1]
        else:
            word = sent[i][0]
            postag = 'A'

        features = [
                'word.lower=' + word.lower(),
                'word[-3:]=' + word[-3:],
                'word[-2:]=' + word[-2:],
                'word[:2]=' + word[:2],
                'word[:3]=' + word[:3],
                'word.isupper=%s' %word.isupper(),
                'word.istitle=%s' %word.istitle(),
                'word.isdigit=%s' %word.isdigit(),
                'word.freq=' + str(freq.get(w,0)),
                'wB=' + bDict.get(word,""),
                'isInDict=' + str(in_dict(word)),
                ]

        for j in range(2,4):
            if i > j-1:
                word1 = sent[i-j][0]
                features.extend([
                '-' + str(j) + ':word.lower='+word1.lower(),
                '-' + str(j) + ':word[-3:]=' + word1[-3:],
                '-' + str(j) + ':word[-2:]=' + word1[-2:],
                '-' + str(j) + ':word[:2]=' + word1[:2],
                '-' + str(j) + ':word[:3]=' + word1[:3],
                '-' + str(j) + ':word.isupper=%s' % word1.isupper(),
                '-' + str(j) + ':word.istitle=%s' % word1.istitle(),
                '-' + str(j) + ':word.isdigit=%s' % word1.isdigit(),
                '-' + str(j) + ':tupp.freq=' + str(freqT.get((word1,word))),
                '-' + str(j) + ':wB=' + bDict.get(word1,""),
                '-' + str(j) + ':isInDict=' + str(in_dict(word1)),
                ])


        if i > 0:
            word1 = sent[i-1][0]
            features.extend([
                '-1:word.lower='+word1.lower(),
                '-1:word[-3:]=' + word1[-3:],
                '-1:word[-2:]=' + word1[-2:],
                '-1:word[:2]=' + word1[:2],
                '-1:word[:3]=' + word1[:3],
                '-1:word.isupper=%s' % word1.isupper(),
                '-1:word.istitle=%s' % word1.istitle(),
                '-1:word.isdigit=%s' % word1.isdigit(),
                '-1:tupp.freq=' + str(freqT.get((word1,word))),
                '-1:wB=' + bDict.get(word1,""),
                '-1:isInDict=' + str(in_dict(word1)),
                ])
        else:
            features.append('BOS')

        
        if i < len(sent) - 1:
            word1 = sent[i+1][0]
            features.extend([
                '+1:word.lower='+word1.lower(),
                '+1:word[-3:]=' + word1[-3:],
                '+1:word[-2:]=' + word1[-2:],
                '+1:word[:2]=' + word1[:2],
                '+1:word[:3]=' + word1[:3],
                '+1:word.isupper=%s' %word1.isupper(),
                '+1:word.istitle=%s' %word1.istitle(),
                '+1:word.isdigit=%s' % word1.isdigit(),
                '+1:tupp.freq=' + str(freqT.get((word,word1))),
                '+1:wB=' + bDict.get(word1,""),
                '+1:isInDict=' + str(in_dict(word1)),
                ])
        else:
            features.append('EOS')

        for j in range(2,4):
            if i < len(sent) - j:
                word1 = sent[i+j][0]
                features.extend([
                 '+' + str(j) + ':word.lower='+word1.lower(),
                 '+' + str(j) + ':word[-3:]=' + word1[-3:],
                 '+' + str(j) + ':word[-2:]=' + word1[-2:],
                 '+' + str(j) + ':word[:2]=' + word1[:2],
                 '+' + str(j) + ':word[:3]=' + word1[:3],
                 '+' + str(j) + ':word.isupper=%s' %word1.isupper(),
                 '+' + str(j) + ':word.istitle=%s' %word1.istitle(),
                 '+' + str(j) + ':word.isdigit=%s' %word1.isdigit(),
                 '+' + str(j) + ':tupp.freq=' + str(freqT.get((word,word1))),
                 '+' + str(j) + ':wB=' + bDict.get(word1,""),
                 '+' + str(j) + ':isInDict=' + str(in_dict(word1)),
                 ])

        print(postag + '\t' + '\t'.join(features))
    print() 
