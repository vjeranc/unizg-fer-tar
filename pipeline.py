#!/usr/bin/env python3
import data_processing as p
import oznacavanje_izraza as oi

import sys
import os
import argparse
import datetime
import subprocess
import pickle
import tarfile
import time 
vectorizer_name = 'vectorizer.pickle'
tfidf_matrix_name = 'tfidf.pickle'
snomed_concepts_list = 'snomed-concepts-list.pickle'
tagger_model_name = 'tagger.model'
word_to_concepts_name = 'word-to-concepts.pickle'
concept_to_words_name = 'concept-to-words.pickle'
wtc_train_name = 'wtc_train.pickle'

tar_dir = 'model'

input_for_fe_name = 'input-for-fe.txt'
input_for_tagger_name = 'input-for-tagger.txt'

output_of_tagged = 'output-of-tagged.txt'
expr_extractor_output = 'output-of-extractor.txt'

main_parser = argparse.ArgumentParser(
    description='Used for training, tagging, evaluating.')

class Directory(argparse.Action):
    """
    Used by argparse to check if the given directory is readable and writable.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        p_dir = values
        if not os.path.isdir(p_dir):
            raise argparse.ArgumentTypeError(
                "directory:{0} is not a valid path".format(p_dir))
        if os.access(p_dir, os.R_OK) and os.access(p_dir, os.W_OK):
            setattr(namespace, self.dest, p_dir)
        else:
            raise argparse.ArgumentTypeError(
                "directory:{0} is not a readable and writable dir".format(
                    p_dir))

subparsers = main_parser.add_subparsers(help='Supported modes of this script.')
train_parser = \
    subparsers.add_parser('train',
                          description='Trains the necessary model '
                                      'from given files.',
                          help='Used for training the whole '
                               'pipeline on provided input.')
tag_parser = \
    subparsers.add_parser('tag',
                          description="""Runs the tagging process
                                   on provided input using given model.""",
                          help='Used for tagging the given files - '
                               'results are stored in .pipe files.')

train_parser. \
    add_argument('--input-folder', '-i',
                 help='Prefix to a directory with a matching .text '
                      'and .pipe files.',
                 metavar='dir-name', required=True,
                 action=Directory, type=str)

train_parser. \
    add_argument('--model-output', '-m',
                 help='Output file name where model will be '
                      'written. Default filename '
                      'is timestamped and stored in current '
                      'directory.',
                 metavar='file-name', required=False,
                 type=argparse.FileType('w+'))

train_parser. \
    add_argument('--snomed-concepts', '-s',
                 help='Path to file where SNOMED concepts are stored. They have'
                      'to be in shortened form (only tag and word).',
                 metavar='file-name', required=True,
                 type=argparse.FileType('r'))

tag_parser. \
    add_argument('--input-folder', '-i',
                 help='Prefix to a directory with a matching .text files.',
                 metavar='dir-name', required=True,
                 action=Directory, type=str)

tag_parser. \
    add_argument('--output-folder', '-o',
                 help='Prefix to a directory where .pipe files will be '
                      'written.',
                 metavar='dir-name', required=True,
                 action=Directory, type=str)

tag_parser. \
    add_argument('--model-path', '-m',
                 help='Output file name from where model will be '
                      'loaded.',
                 metavar='file-name', required=True,
                 type=argparse.FileType('r'))

tag_parser. \
    add_argument('--tagged',
                 help='Indicating that data is tagged and should be '
                      'processed differently.',
                 required=False,
                 action='store_true'
                )


def tagger_learn_args(model_path, input_path):
    return  ['./crfsuite'
            , 'learn'
            , '-m'               # model output file
            , model_path
            #, '-a', 'l2sgd'
            #, '-p', 'max_iterations=1000'
            , input_path]


def tagger_tag_args(model_path):
    return  ['./crfsuite'
            , 'tag'
            , '-m'               # model output file
            , model_path
            ]


def feature_extractor_args(input_path):
    return ['python3'
           , 'skripta.py'
           , input_path]


def extractor_expression_args(input_path):
    return ['python3',
            'pretvaranje.py',
            input_path]


def train(args):
    in_dir = args.input_folder

    timestamp = str(int(datetime.datetime.now().timestamp()))

    work_dir_name = '.temporary'+timestamp
    os.mkdir(work_dir_name)  # creating the hidden working directory
    print("Generating input...")
    input_for_fe = os.path.join(work_dir_name, input_for_fe_name)
    p.generate_input_file_from_sentences(input_for_fe, in_dir)
    print("Done!")

    # feature extracting

    print("Extracting features for tagger training...")
    input_for_tagger = os.path.join(work_dir_name, input_for_tagger_name)
    process = subprocess.Popen(feature_extractor_args(input_for_fe),
                               stdout=open(input_for_tagger, 'w+'))
    process.wait()
    print("Done!")

    # tagger training
    print("Training has started!")
    '''tagger_model_out = os.path.join(work_dir_name, tagger_model_name)
    process = subprocess.Popen(tagger_learn_args(tagger_model_out, input_for_tagger),
                               stdout=subprocess.PIPE)
    for line in iter(process.stdout.readline, ''):
        line = line.decode()
        if not line: break
        print(line)
    '''
    time.sleep(5)
    print("Tagger done!")

    print("Extracting features from SNOMED concepts.")
    snomed_file = args.snomed_concepts
    snomed_file.close()  # have to close it it'll be opened next
    sorted_snomed_concepts, word_to_concepts, concept_to_words, v, matrix, wtc_train = oi.build_vectorizer(
        snomed_file.name)

    vectorizer_output = os.path.join(work_dir_name, vectorizer_name)
    tfidf_matrix_output = os.path.join(work_dir_name, tfidf_matrix_name)
    snomed_concepts_output = os.path.join(work_dir_name, snomed_concepts_list)
    word_to_concepts_path = os.path.join(work_dir_name, word_to_concepts_name)
    concept_to_words_path = os.path.join(work_dir_name, concept_to_words_name)
    wtc_train_path = os.path.join(work_dir_name, wtc_train_name)
    with open(vectorizer_output,'wb+') as file:
        pickle.dump(v, file)
    with open(tfidf_matrix_output,'wb+') as file:
        pickle.dump(matrix, file)
    with open(snomed_concepts_output,'wb+') as file:
        pickle.dump(sorted_snomed_concepts, file)
    with open(word_to_concepts_path,'wb+') as file:
        pickle.dump(word_to_concepts, file)
    with open(concept_to_words_path,'wb+') as file:
        pickle.dump(concept_to_words, file)
    with open(wtc_train_path,'wb+') as file:
        pickle.dump(wtc_train,file)
    print("SNOMED concepts learned")

    print("Saving model...")
    model_out = args.model_output
    model_out = ('model'+timestamp) if not model_out else model_out

    # model saved in a giant tar

    tar = tarfile.open(model_out+".tar.gz", "w:gz")
    tar.add(work_dir_name, arcname=tar_dir)
    tar.close()

    print("Model saved!")

    print("Cleaning...")
    for file in os.listdir(work_dir_name):
        os.unlink(os.path.join(work_dir_name, file))

    os.rmdir(work_dir_name)
    print("Done!")

train_parser.set_defaults(func=train)


def tag(args):
    model_path = args.model_path
    model_path.close()
    input_path = args.input_folder

    timestamp = str(int(datetime.datetime.now().timestamp()))

    work_dir_name = '.temporary'+timestamp
    os.mkdir(work_dir_name)  # creating the hidden working directory
    print("Generating input...")
    fe_input = os.path.join(work_dir_name, input_for_fe_name)
    if args.tagged:
        indices = p.generate_input_file_from_sentences(fe_input,
                                                       input_path)
    else:
        indices = p.generate_input_file_from_pipeless_documents(fe_input,
                                                                input_path)
    print("Done!")
    model_tar = tarfile.open(model_path.name, 'r:gz')

    temp_model_path = os.path.join(work_dir_name, tagger_model_name)
    temp_model = open(temp_model_path, 'wb+')
    with model_tar.extractfile(tar_dir + '/' + tagger_model_name) as f:
        temp_model.write(f.read())
    temp_model.close()

    # feature extraction
    print("Extracting features and preparing input for tagging...")

    tagger_input = os.path.join(work_dir_name, input_for_tagger_name)
    process = subprocess.Popen(feature_extractor_args(fe_input),
                               stdout=open(tagger_input, 'w+'))
    process.wait()

    print("Finished!")
    # tagging

    print("Tagging started...")

    tagger_output = os.path.join(work_dir_name, output_of_tagged)
    process = subprocess.Popen(
        tagger_tag_args(temp_model_path),
        stdout=open(tagger_output, 'w+'),
        stdin=open(tagger_input))
    process.wait()

    print("Tagging finished!")

    print("Extracting medical expressions from tagger output...")
    input_for_extractor = tagger_output
    extractor_output = os.path.join(work_dir_name, expr_extractor_output)
    process = subprocess.Popen(extractor_expression_args(input_for_extractor),
                               stdout=open(extractor_output, 'w+'))
    process.wait()
    print("Done!")

    print("Marking the expression with SNOMED concept and writing .pipe files")
    with model_tar.extractfile(tar_dir+'/'+vectorizer_name) as f:
        vectorizer = pickle.load(f)
    with model_tar.extractfile(tar_dir+'/'+snomed_concepts_list) as f:
        snomed_concepts = pickle.load(f)
        snomed_concepts = ({k: i for k, i in
                           zip(snomed_concepts, range(len(snomed_concepts)))},
                           [x for x in snomed_concepts])
    with model_tar.extractfile(tar_dir+'/'+tfidf_matrix_name) as f:
        tfidf_matrix = pickle.load(f)
    with model_tar.extractfile(tar_dir+'/'+word_to_concepts_name) as f:
        wtc = pickle.load(f)
    with model_tar.extractfile(tar_dir+'/'+concept_to_words_name) as f:
        ctw = pickle.load(f)
    with model_tar.extractfile(tar_dir+'/'+wtc_train_name) as f:
        wtc_train = pickle.load(f)

    exprss = p.read_extractor_indices(extractor_output)
    output_path = args.output_folder
    for sent_id, token_idss in sorted(exprss.items()):
        file_name, tokens = indices[sent_id]
        file_path = os.path.join(input_path, file_name)
        file_text = p.read_document(file_path)
        out_file_path = os.path.join(output_path,
                                     file_name.replace('.text', '.pipe'))
        with open(out_file_path, 'a') as out:
            for token_ids in token_idss:

                ranges = [tokens[id] for id in token_ids if id < len(tokens)]
                full_expr = ' '.join(file_text[a:b] for a, b in ranges)
                best_concept = oi.calculate_similarity(snomed_concepts,
                                                       vectorizer,
                                                       tfidf_matrix,
                                                       wtc, ctw,
                                                       full_expr,wtc_train)

                if not p.is_distant(token_ids):
                    ranges = [(ranges[0][0], ranges[-1][1])]

                out.write("{}|{}|{}\n".format(file_name,
                                              ','.join("{}-{}".format(a, b) for
                                                       a, b in ranges),
                                              best_concept))
    print("Done!")

    print("Cleaning up...")
    for file in os.listdir(work_dir_name):
        os.unlink(os.path.join(work_dir_name, file))

    os.rmdir(work_dir_name)
    print("Done!")



tag_parser.set_defaults(func=tag)

if __name__ == "__main__":
    margs = main_parser.parse_args(sys.argv[1:])
    margs.func(margs)
