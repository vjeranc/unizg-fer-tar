import pickle
import sklearn.feature_extraction.text as fe
from sklearn.metrics.pairwise import linear_kernel


def load_concepts(file_path):
    concepts = []
    with open(file_path, 'r') as file:
        for line in file:
            concept, words = line.split(maxsplit=1)
            concepts.append((concept, words.strip()))

    return concepts


def my_cleaning_function(x):
    return x.strip('().,:[]').lower()


def construct_concept_map_to_words(concepts, mm={}, cleaning=lambda x: x):
    """
    Concepts are tuples of (concept_string, word_string).
    :param concepts: list of tuples
    :param mm: an existing map can be given and it will be updated with new
                concepts (default empty)
    :param cleaning: function that processes individual words (default nothing)
    :return: map with concept_string key and a set of words describing a
    concept
    """
    for key, words in concepts:
        if key not in mm:
            mm[key] = set()
        split_words = words.strip().split()
        split_words.append(str(len(split_words)))
        mm[key].update(map(cleaning,split_words))

    return mm


def construct_word_to_concepts_map(concepts, mm={}, cleaning=lambda x: x):
    """
    Returns a word to concepts map
    :param concepts: list of tuples
    :param mm: map that can be updated
    :return: map of word to concepts
    """
    for concept, words in concepts:
        for word in map(cleaning, words.strip().split()):
            if word not in mm:
                mm[word] = set()
            mm[word].add(concept)

    return mm

def construct_words_to_concepts_map(concepts, mm={}, cleaning=lambda x: x):
    """
    Returns a word to concepts map
    :param concepts: list of tuples
    :param mm: map that can be updated
    :return: map of word to concepts
    """
    for concept, words in concepts:
        words_list = ' '.join(sorted( map(cleaning, words.strip().split())))
        if words_list not in mm:
            mm[words_list] = set()
        mm[words_list].add(concept)

    return mm


def td_idf(map_to_words, word_to_concepts,wtc_train):
    docs = {}
    for key, words in map_to_words.items():
        docs[key] = ' '.join(words)

    vectorizer = fe.TfidfVectorizer(max_df=0.9)
    sorted_docs = [k for k in sorted(docs)]
    return (sorted_docs,
            word_to_concepts,
            map_to_words,
            vectorizer,
            vectorizer.fit_transform(docs[key] for key in sorted_docs),wtc_train)


def build_vectorizer(file_path):
    concepts = load_concepts(file_path)
    concepts_train = set()
    with open("oznake_train","r") as file:
        for line in file:
            concepts_train.add(line.strip())

    ctw = construct_concept_map_to_words(concepts, cleaning=my_cleaning_function)
    wtc = construct_word_to_concepts_map(concepts, cleaning=my_cleaning_function)
    wtc_train = construct_words_to_concepts_map(filter(lambda x: x[0] in concepts_train, concepts), cleaning=my_cleaning_function)
    print("Number of concepts: {:d}".format(len(ctw)))
    return td_idf(ctw, wtc,wtc_train)


def select_concepts(snomed_concepts, wtc, ctw, document, should_filter):
    concepts = set()
    words = set(map(my_cleaning_function, document.split()))
    words.add(str(len(words)))
    if should_filter:
        f = lambda x: words.issubset(ctw[x])
    else:
        f = lambda x: True
    for word in words:
        wconcepts = filter(f, wtc.get(my_cleaning_function(word), []))
        concepts.update(wconcepts)
    sorted_sn_concepts = sorted(snomed_concepts[concept] for concept in concepts)
    indices = dict(zip(range(len(sorted_sn_concepts)), sorted_sn_concepts))
    return indices


def calculate_similarity(snomed_concepts, vectorizer, tfidf_matrix, wtc, ctw,
                         document,wtc_train):

    #if not concept_indices_map:
    #   concept_indices_map = select_concepts(snomed_concepts[0], wtc, ctw,
    #                                        document, False)

    words = ' '.join(sorted( map(my_cleaning_function, document.strip().split())))
    if words in wtc_train:
        cui_set = wtc_train[words]
        if len(cui_set) == 1:
            return next(iter(cui_set))
        else:
            sorted_sn_concepts = sorted(snomed_concepts[0][concept] for concept in cui_set)
            concept_indices_map = dict(zip(range(len(sorted_sn_concepts)), sorted_sn_concepts))
            matrix_indices = list(concept_indices_map.values())
    else:
        concept_indices_map = select_concepts(snomed_concepts[0], wtc, ctw,
                                          document, True)

        if not concept_indices_map:  # if there are no CUIs
            return "CUI-less"

        matrix_indices = list(concept_indices_map.values())

    dot_matrix = tfidf_matrix[matrix_indices]
    doc_vector = vectorizer.transform([document])
    cosine_similarities = linear_kernel(doc_vector, dot_matrix).flatten()

    most_related_concept = cosine_similarities.argsort()[-1]
    most_related_concept_index = concept_indices_map[most_related_concept]
    return snomed_concepts[1][most_related_concept_index]
