#!/usr/bin/env python3

import nltk.tokenize as tok
import itertools as it
import os
import sys
import pickle
import ast

text_test = 'clinical-ie-tar-project/train/00098-016139.text'
pipe_test = 'clinical-ie-tar-project/train/00098-016139.pipe'


def char_based_sequence(text_path, pipe_path):
    text_string = read_document(text_path)
    pipe_string = read_document(pipe_path)
    ar = pipe_to_indexed_array(pipe_string)
    for c, description in zip(text_string, ar):
        print(c, description)


def read_document(path):
    with open(path) as file:
        return file.read()


def simplify_snomed(snomed_path, output_file, lang=lambda x: x == 'ENG'):
    """
    SNOMED-CT-CONCEPTS.TXT contains a lot of data, for the first task only
    the tag and the word is needed. This function simplifies the concepts.
    :param snomed_path: path to the snomed concepts
    :param output_file: output of the simplified
    """
    with open(snomed_path) as snomed_file:
        with open(output_file, 'w') as out:
            for line in snomed_file:
                l = line.split('|')
                if lang(l[1]):
                    out.write("{} {}\n".format(l[0], l[14]))


def extract_all_used_concepts(path_to_all_pipe_files, output_file):
    """
    Extracts all used snomed concepts from .pipe files. It's unnecessary to
    train the models using data that does not exist in the corpus.
    :param path_to_all_pipe_files: directory containing .pipe files
    :param output_file: path to the file where all the concepts will be written
    """
    files = filter(lambda x: x.endswith('.pipe'),
                   os.listdir(path_to_all_pipe_files))
    concepts = set()

    for file_path in files:
        file_path = os.path.join(path_to_all_pipe_files, file_path)
        with open(file_path) as file:
            for line in file:
                l = line.split('|',3)
                concepts.add(l[2])

    with open(output_file, 'w+') as out:
        for concept in concepts:
            out.write("{}\n".format(concept))


def generate_input_file_from_sentences(output_path, directory_prefix):
    files = os.listdir(directory_prefix)
    files = set(x[:-5] for x in files)
    indices = []
    with open(output_path, 'w') as out_file:
        for file in files:
            text_name = file+'.text'
            text_path = os.path.join(directory_prefix, text_name)
            pipe_path = os.path.join(directory_prefix, file + '.pipe')
            text_string = read_document(text_path)
            seq = word_based_sentence_sequence(text_path, pipe_path)
            for sent in seq:
                l = []
                indices.append((text_name, l))
                for (start, end), tags in sent:
                    l.append((start, end))
                    if not tags:
                        tag = 'O'
                    else:
                        tag = tags.pop()[2]
                    out_file.write("{:}\t{:}\n".format(text_string[start:end], tag))
                out_file.write("\n")
    return indices


def generate_input_file_from_documents(output_path, directory_prefix):
    """
    Generates the input files "WORD\tTAG" where possible tags are B, I, DB, DI
    and O. Documents are separated by
    :param output_path:
    :param directory_prefix:
    :return:
    """
    files = os.listdir(directory_prefix)
    files = set(x[:-5] for x in files)
    with open(output_path, 'w') as out_file:
        for file in files:
            text_path = os.path.join(directory_prefix, file + '.text')
            pipe_path = os.path.join(directory_prefix, file + '.pipe')
            text_string = read_document(text_path)
            for (start, end), tags in word_based_sequence(text_path, pipe_path):
                if not tags:
                    tag = 'O'
                else:
                    word_info = tags.pop()
                    expr = " ".join(text_string[a:b] for a, b in word_info[3])
                    expr = " ".join(expr.split())
                    print(word_info[1], expr)
                    tag = word_info[2]
                out_file.write("{:}\t{:}\n".format(text_string[start:end], tag))
            out_file.write("\n")


def generate_input_file_from_pipeless_documents(output_path, directory_prefix):
    tokenizer = tok.WordPunctTokenizer()
    sentence_tokenizer = tok.PunktSentenceTokenizer()

    indices = []
    with open(output_path, 'w+') as output:
        for file in filter(lambda x: x.endswith('.text'),
                           os.listdir(directory_prefix)):
            file_path = os.path.join(directory_prefix, file)
            text_string = read_document(file_path)
            ids = sentence_tokenizer.span_tokenize(text_string)
            for start, end in ids:
                l = []
                indices.append((file, l))
                token_indices = tokenizer.span_tokenize(text_string[start:end])
                for a, b in token_indices:
                    l.append((start+a, start+b))
                    output.write(text_string[start+a:start+b].strip())
                    output.write('\n')
                output.write('\n')
    return indices


def word_based_sequence(text_path, pipe_path, remove_broken=True):
    """
    Reads the pipe tags and original text returning tokenized document where
    each token has its assigned tags.
    :param text_path: path to the medical text
    :param pipe_path: path to the tags of the medical text
    :param remove_broken: removing tags from broken words (more than one tag)
    :return: sorted list of tokens (from first one to last) with corresponding
             tags
    """
    tokenizer = tok.WordPunctTokenizer()
    text_string = read_document(text_path)
    pipe_string = read_document(pipe_path)
    indices = tokenizer.span_tokenize(text_string)
    word_ranges = {}
    for ranges_list, identifier in pipe_ranges_identifier(pipe_string):
        first = True
        start = ranges_list[0][0]
        end = ranges_list[-1][1]
        marker = (start, end)
        tup_ranges_list = tuple(ranges_list)
        for word_range in ranges_list:
            l = word_ranges.get(word_range, set())
            if not l:
                word_ranges[word_range] = l
            if first:
                l.add((
                    marker, identifier, '' if len(ranges_list) == 1 else 'D',
                    tup_ranges_list))
                first = False
            else:
                l.add((
                    marker, identifier, '' if len(ranges_list) == 1 else 'D',
                    tup_ranges_list))

    sorted_word_ranges = sorted(word_ranges.items())
    if remove_broken:
        set_broken_words_tags_to_empty_single_sentence(
            list(sorted_word_ranges))

    word_ranges = list(reversed(sorted_word_ranges))
    ((wstart, wend), identifier), first = word_ranges.pop(), True
    word_identifier_map = {}  # collecting all the words
    seen_markers = set()      # if marker is seen then 'I' else 'B' for tag
    for start, end in indices:
        l = word_identifier_map.get((start, end), set())
        w_identifier = []
        while start > wend and word_ranges:
            ((wstart, wend), identifier), first = word_ranges.pop(), True

        if wstart <= start and end <= wend:
            w_identifier = set(
                (marker,
                 identifier,
                 x + 'B' if marker not in seen_markers else x + 'I',
                 ranges_list)
                for marker, identifier, x, ranges_list in identifier)

        if not l:
            word_identifier_map[(start, end)] = l

        for (marker, _, _, _) in w_identifier:
            seen_markers.add(marker)

        l.update(w_identifier)
    return sorted(word_identifier_map.items())


def word_based_sentence_sequence(text_path, pipe_path, merge_sentences=True):
    """
    Reads the pipe tags and original text returning tokenized document where
    each token has its assigned tags.
    :param text_path: path to the medical text
    :param pipe_path: path to the tags of the medical text
    :param merge_sentences: merge sentences having tags spanning across
    :return: sorted list of tokens (from first one to last) with corresponding
             tags
    """
    word_identifier_list = word_based_sequence(text_path, pipe_path)
    text_string = read_document(text_path)

    sentence_tokenizer = tok.PunktSentenceTokenizer()
    sent_indices = list(reversed(sentence_tokenizer.span_tokenize(text_string)))

    sentences, sentence = [], []
    _, sent_end = sent_indices.pop()

    for (start, end), tags in word_identifier_list:
        if start >= sent_end:
            sentences.append(sentence)
            sentence = []
            _, sent_end = sent_indices.pop()

        sentence.append(((start, end), tags))

    faulty_sent_indices = check_if_tags_go_across_sentences(sentences)
    while faulty_sent_indices:
        merge_map = {}
        for index in faulty_sent_indices:
            index_set1 = merge_map.get(index, set())
            index_set2 = merge_map.get(index+1, set())
            if not index_set1:
                merge_map[index] = index_set1
            if not index_set2:
                merge_map[index+1] = index_set2
            index_set1.add(index+1)
            index_set2.add(index)
        for index in range(len(sentences)):
            if index not in merge_map:
                merge_map[index] = set([index])

        connected_sentences = components(merge_map)
        new_sentences = []
        for component in connected_sentences:
            merged_sentences = []
            for index in sorted(component):
                merged_sentences.extend(sentences[index])
            new_sentences.append(merged_sentences)
        sentences = new_sentences
        faulty_sent_indices = check_if_tags_go_across_sentences(sentences)

    return sentences


def walk(G, s, S=set()):
    """
    Breadth-first traversal of a graph.
    :param G: graph (mapping from index to set of indices)
    :param s:
    :param S:
    :return:
    """
    P, Q = dict(), set()
    P[s] = None
    Q.add(s)
    while Q:
        u = Q.pop()

        for v in G[u].difference(P, S):
            Q.add(v)
            P[v] = u

    return P


def components(G):
    """
    Returns the connected components of a graph.
    :param G:
    :return:
    """
    comp = []
    seen = set()
    for u in G:
        if u in seen: continue
        C = walk(G, u)
        seen.update(C)
        comp.append(C)
    return comp


def check_if_tags_go_across_sentences(sentences):
    """
    Prints out if there's any divided medical expressions ranging through
    several sentences.
    :param sentences:
    :return: sentence indices
    """
    across = set()
    for sent, i in zip(sentences, it.count()):
        end_index = sent[-1][0][1]
        for _, tags in sent:
            for tag in tags:
                end_index_tag = tag[0][1]
                if end_index < end_index_tag:
                    across.add(i)
    return across


def set_broken_words_tags_to_empty_single_sentence(sent):
    for (s, tags), i in zip(sent, it.count()):
        if len(tags) > 1:
            tags_copy = tags.copy()  # shallow copy (we're not changing elems)
            for ph, stags in sent[:i]:
                tags_copy.difference_update(stags)  # retain any different elem
                stags.difference_update(tags)       # retain any different elem
            for ph, stags in sent[i + 1:]:
                tags_copy.difference_update(stags)  # retain any different elem
                stags.difference_update(tags)       # retain any different elem
            tags.clear()                            # clear all elems
            tags.update(tags_copy)                  # copy leftovers


def set_broken_words_tags_to_empty(sentences):
    """
    Words having more than one tag and all of the words that share their tag
    will have their tag sets set to empty ones. get_broken(sentences) should
    output an empty set after usage of this function.
    :param sentences:
    """
    for sent, si in zip(sentences, it.count()):
        set_broken_words_tags_to_empty_single_sentence(sent)


def get_broken_single_sentence(sent, si=None, n=set()):
    for (_, tags), i in zip(sent, it.count()):
        if len(tags) > 1:
            for se, stags in sent[:i]:
                if not tags.isdisjoint(stags):
                    n.add((se, i, si))
            for se, stags in sent[i + 1:]:
                if not tags.isdisjoint(stags):
                    n.add((se, i, si))
    return n


def get_broken(sentences):
    """
    Retrieves the number of words that take part in a medical expression that
    is broken (having more than one possible tag for it).
    :param sentences: tagged sequence
    :return: broken tokens in the list of sentences ((start, end), sentence_id)
    """
    n = set()
    for sent, si in zip(sentences, it.count()):
        get_broken_single_sentence(sent, si, n)
    return n


def count_tagged(sentences):
    """
    Counts the number of properly tagged words in the sequence of sentences.
    Properly tagged words are those containing at least some tags (they are
    part of medical expressions)
    :param sentences:
    :return:
    """
    n = 0
    for sent in sentences:
        for _, tags in sent:
            if tags:
                n += 1
    return n


def pipe_ranges_identifier(pipe_string):
    for description in pipe_string.splitlines():
        _, char_ranges, identifier = description.split('|')[:3]
        ranges_list = []
        for char_range in char_ranges.split(','):
            start, end = char_range.split('-')
            start, end = int(start), int(end)
            ranges_list.append((start, end))
        yield (ranges_list, identifier)


def pipe_to_indexed_array(pipe_string):
    """

    :param pipe_string: string
    :return:
    """
    n = len(pipe_string)
    arr = [[] for _ in range(n)]
    for char_ranges, identifier in pipe_ranges_identifier(pipe_string):
        beginning = True
        for char_range in char_ranges:
            start, end = char_range
            for i in range(start, end): # this is correct range
                if beginning:
                    arr[i].append('B')
                    beginning = False
                else:
                    arr[i].append('I')
                arr[i].append(identifier)

    return arr


def read_extractor_indices(file_path):
    mm = {}
    with open(file_path) as f:
        for line in f:
            n, arr = line.split('\t', 1)
            arr = ast.literal_eval(arr)
            l = []
            mm[int(n)] = l
            for el in arr:
                if el:
                    expr = map(lambda x: int(x), el.split(','))
                    l.append(list(expr))

    return mm


def is_distant(x):
    a, b = x[0], x[-1]
    return (b-a+1) != len(x)

if __name__ == "__main__":
    output = sys.argv[1]
    input_prefix = sys.argv[2]
    generate_input_file_from_documents(output, input_prefix)