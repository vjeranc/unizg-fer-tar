import os
import sys

precision = []
recall = []
scores = []

for f in os.listdir(sys.argv[1]):
    if f.endswith(".pipe"):
        fo = open(sys.argv[1]+f,"r")
        iModel = []
        iTocno = []
        for line in fo:
            arr = line.strip().split("|")
           # if arr[2][0]=='{':
            #    arr[2] = ''.join(arr[2][2:-2])
             #   print(arr[2])
            iModel.append(arr[0] + "|" + arr[1] + "|" + arr[2].lower())
            #iModel.append(arr[1])

        fo = open(sys.argv[2]+f)
        for line in fo:
            arr = line.split("|")
            iTocno.append(arr[0] + "|" + arr[1] + "|" + arr[2].lower())
            #iTocno.append(arr[1])

        tp=0
        fp=0
        tn=0
        fn=0

        for line in iModel:
            if line in iTocno:
                tp+=1
            else:
                fp+=1
        for line in iTocno:
            if not line in iModel:
                fn+=1

    precision.append(float(tp)/float(tp + fp))
    recall.append(float(tp)/float(tp+fn))
    scores.append(float(2*tp)/(2*tp+fp+fn))
print(sum(precision) / len(precision))
print(sum(recall) / len(recall))
print(sum(scores) / len(scores))
