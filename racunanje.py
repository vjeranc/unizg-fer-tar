import sys

f1 = open(sys.argv[1],"r").readlines()
f2 = open(sys.argv[2],"r").readlines()

comm = 0

dict_match = {}
dict_model = {}
dict_ref = {}
for t1,t2 in zip(f1,f2):
    if len(t1.strip()) < 1:
        continue;
    
    a = t1.strip().split("\t")[1]
    b = t2.strip().split("\t")[1]
    
    #print(a + "\t" + b)
    dict_model[b] = dict_model.get(b,0) + 1
    dict_ref[a] = dict_ref.get(a,0) + 1

    if a == b:
        dict_match[a] = dict_match.get(a,0) + 1

print ("Performance by label (#match,#model,#ref) (precision)")

for k in dict_match:
    match = dict_match[k]
    model = dict_model[k]
    ref = dict_ref[k]

    print(k + ": (" + str(match) +',' + str(model) +',' + str(ref)+')' 
            +' ( ' + str(float(match) / float(ref)) + ')')
